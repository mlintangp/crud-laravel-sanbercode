@extends('adminlte.master')

@section('title')
    Detail Cast
@endsection

@section('content')

<div class="ml-3">
    <h2>Detail Data Cast</h2>
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="card-title">Detail Data Cast</h3>
        </div>
        <div class="card-body">
            <h1>{{ $cast->nama }}</h1>
            <p>Umur: {{ $cast->umur }} Tahun</p>
            <p>{{ $cast->bio }}</p>
        </div>
    </div>
</div>

@endsection