@extends('adminlte.master')

@section('title')
    Edit Data Cast
@endsection

@section('content')

<div class="ml-3">
    <h2>Edit Data Cast</h2>
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="card-title">Edit Data Cast</h3>
        </div>
        <div class="card-body">
            <form action="/cast/{{$cast->id}}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" value="{{$cast->umur}}" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">Edit</button>
            </form>
        </div>
    </div>
</div>

@endsection