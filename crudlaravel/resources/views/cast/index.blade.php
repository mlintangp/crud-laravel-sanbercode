@extends('adminlte.master')

@section('title')
    Data Cast
@endsection

@section('content')

<div class="ml-3">
    <h2>Data Cast</h2>
    <div class="card mt-3">
        <div class="card-header">
            <h3 class="card-title">Data Para Cast</h3>
        </div>
        <div class="card-body">  
            <a href="/cast/create" class="btn btn-success btn-sm">Tambah Data Cast</a>
            <table class="table table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($cast as $key => $c)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $c->nama }}</td>
                        <td>{{ $c->umur }}</td>
                        <td>{{ $c->bio }}</td>
                        <td>
                            <form action="/cast/{{ $c->id }}" method="POST">
                                @csrf
                                @method('delete')
                                <a href="/cast/{{ $c->id }}" class="btn btn-info btn-sm">Details</a>
                                <a href="/cast/{{ $c->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>

                @empty
                    <tr>
                        <td>Tidak ada data yang dapat ditampilkan.</td>
                    </tr>
                @endforelse
            </tbody>
            </table>
        </div>
    </div>
</div>

@endsection